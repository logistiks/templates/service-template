import { Logger } from '@logistiks/utils';
import Service from './service';

(async () => {
  let service: Service = new Service();
  await service.start();

  // Logger.info(`Server - Up and running at http://${process.env.HOST}:${process.env.PORT}`);
})();

// listen on SIGINT signal and gracefully stop the server
process.on('SIGINT', () => {
  Logger.info('Stopping service');

  // Server.stop().then(err => {
  //   Logger.info(`Server stopped`);
  //   process.exit(err ? 1 : 0);
  // });
});