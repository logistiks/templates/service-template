
import HemeraJaeger from 'hemera-jaeger';
import HemeraJoi from 'hemera-joi';
import { connect, Client } from 'nats';
import Hemera from 'nats-hemera';
import { manifest, Logger, SystemUtils, SharedLogic, Database } from '@logistiks/utils';
import { ServiceActions } from './service.actions';
 
export default class Service {
  private _instance: Hemera<Hemera.ServerRequest, Hemera.ServerResponse>;
  private _nats: Client;

  constructor() {
    this._nats = connect(manifest.get('/nats'));
    this._instance = new Hemera(this._nats, manifest.get('/hemera/conf'));
  }

  public async start(): Promise<void> {
    try {
      await this.init();

      this._instance.use(HemeraJoi);
      this._instance.use(HemeraJaeger, manifest.get('/hemera/jaegger'));

      await this._instance.ready();
      const sharedLogic = new SharedLogic();

      // const userRepository = new UserRepository();
      // const userService = new UserService(userRepository);

      // Register service actions
      const serviceActions = new ServiceActions(this._instance, this._instance.joi, sharedLogic);
      serviceActions.registerActions();

      Logger.debug(`${process.env.SERVICE_NAME} listening...`);
    } catch (error) {
      Logger.info(`Service ${process.env.SERVICE_NAME} - There was something wrong: ${error}`);
    }
  }

  private async init(): Promise<void> {
    
    if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production') {
      Logger.debug('Waiting 30 seconds for database...');
      await SystemUtils.sleep(30000);
    } else {
      // Logger.debug(`Auth service config: ${JSON.stringify(config.getServerConfig())}`);
    }

    // Connect to the database
    const database: Database = new Database();
    try {
      await database.setupDatabase();
    } catch (error) {
      Logger.error(`Database connection failed: ${JSON.stringify(error)}`);
    }
  }
}